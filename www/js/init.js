		/* Слайдер */

$(function(){
	$("#sliderHorizontal1").bxSlider({
		maxSlides: 3,
		slideWidth: 280,
		slideMargin: 20,
		moveSlides: 1,
		pager: false,
		nextText: "Следующий слайд",
		prevText: "Предыдущий слайд",
		auto: false,
		speed: 600
	});
	$("#sliderHorizontal2").bxSlider({
		maxSlides: 3,
		slideWidth: 280,
		slideMargin: 20,
		moveSlides: 1,
		pager: false,
		nextText: "Следующий слайд",
		prevText: "Предыдущий слайд",
		auto: false,
		speed: 600
	});
});

		/* Order Accordion */

$(function(){
	var rer1;
	$(".orderBoxAccor").each(function(){
		rer1 = $(this).find("li").eq(0).innerHeight()+$(this).find("li").eq(1).innerHeight()+$(this).find("li").eq(2).innerHeight();
		$(this).css({"height": rer1});
	});
	$(".orderBoxLeftContent>span").on("click", function(){
		if($(this).hasClass("openOrderA")) {
			$(this).removeClass("openOrderA").empty().html("Показать все товары");
			rer1 = $(this).prev("ul").find("li").eq(0).innerHeight()+$(this).prev("ul").find("li").eq(1).innerHeight()+$(this).prev("ul").find("li").eq(2).innerHeight();
			$(this).prev("ul").css({"height": rer1});
		} else {
			$(this).addClass("openOrderA").empty().html("Свернуть");
			$(this).prev("ul").css({"height": "auto"});
		}
	});
});

		/* Order Accordion2 */

$(function(){
	$(".modalCatItemContenLink").on("click", function(){
		if($(this).hasClass("openDescr")) {
			$(this).removeClass("openDescr");
			$(this).prev(".modalCatItemContenText").css({"height": "40"});
		} else {
			$(this).addClass("openDescr");
			$(this).prev(".modalCatItemContenText").css({"height": "auto"});
		}
	});
});

		/* Валидация формы */

$(function(){
	$("form").validationEngine({
		scroll:false	
	});
});

		/* Form styler */

$(function(){
	setTimeout(function() {
		$("select").styler({
			selectSmartPositioning: false
		});
	}, 100);
});

		/* Datepicker */

$(function(){
	$("input.datePick").datepicker({
		format: "dd.mm.yyyy",
		language: "ru",
		orientation: "bottom right",
		autoclose: true,
		todayHighlight: true
	});
});

		/* Modal Order1 */

$(function(){
	$(".modalCat>li>a").on("click", function(){
		$(this).next(".scrollModalCat").css({"display": "block"});
		$(".closeItemModal").css({"display": "block"});
	});
	$(".closeItemModal").on("click", function(){
		$(".scrollModalCat").css({"display": "none"});
		$(".closeItemModal").css({"display": "none"});
	});
});

		/* jqueryScrollbar */

$(function(){
	$(".scrollbar-macosx").scrollbar({
		disableBodyScroll: true
	});
});